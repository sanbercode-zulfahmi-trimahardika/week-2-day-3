<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tugas 3</title>
</head>
<body>
    <?php
        function ubah_huruf($string){
        //kode di sini
        $arrayHuruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',];

        $kataBaru = "";
        for ($i = 0; $i < strlen($string); $i++) {
            $cari = array_search(substr($string, $i, 1), $arrayHuruf);
            $kataBaru = $kataBaru.$arrayHuruf[$cari+1];
        }
        return $kataBaru;
        }

        // TEST CASES
        echo ubah_huruf('wow') . "<br>"; // xpx
        echo ubah_huruf('developer') . "<br>"; // efwfmpqfs
        echo ubah_huruf('laravel') . "<br>"; // mbsbwfm
        echo ubah_huruf('keren') . "<br>"; // lfsfo
        echo ubah_huruf('semangat') . "<br>"; // tfnbohbu
    ?>
</body>
</html>
