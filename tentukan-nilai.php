<?php
function tentukan_nilai($number)
{
	$nilai = $number;
    if ($nilai >= 85 && ($nilai <= 100))  {
    	return 'Sangat Baik' . '<br>';
    } elseif ($nilai >= 70 && ($nilai < 85)) {
    	return 'Baik' . '<br>';
    } elseif ($number >= 60 && ($nilai < 70)) {
    	return 'Cukup' . '<br>';
    } else {
    	return 'Kurang' . '<br>';
    }
    
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>